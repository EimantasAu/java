
package lt.vcs;

import java.util.Date;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Banknotas.*;
import lt.vcs.Gender;
import lt.vcs.Person;

public class Main {
    private static final String PIN = "test";
    
    private static String programName; 
    private static Date creationDate;
    private static String creator;
    
    public Main(String name, String programmer) {
        this(name);
        this.creator = programmer;
    }
    
    public Main(String name) {
        
        super();
        
       if (name == null || name.isEmpty()) {
           out("Programa turi tureti pavadinima");
           System.exit(-1);
          
       }
        this.programName = name;
        this.creationDate = new Date();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        out(programName);
     int likutis = 350;
     String pin = inStr("Iveskite PIN koda");
     if ("test".equals(pin)){
         //jeigu teisingas pin - vykdomas kodas
         
         out("Jusu saskaitoje" + likutis + "pinigu");
         out("Galite pasirinkti tik vieno nominalo banknota");
         out("banknotu nominalai");
         for( Banknotas bnkn : Banknotas.values()) {
             out(bnkn.getLabel());
         }
         int isemam = inInt("Kiek norite issigryninti?");
    
         if(isemam <= 0 || isemam > likutis) {
             out("Neteisinga suma");             
         }
         else
         { 
             Banknotas bnkn = suraskBanknota(isemam);
             out("Jums issgryninta " + bnkn.getLabel() + " pinigu");
             out("Jusu saskaitos likutis" + (likutis - bnkn.getSk()));
         } 
     }
         else {
         out("Kodas neteisingas");
     }
     //DVIM.getSk();
     String name = inStr ("Iveskite varda");
         String surname = inStr ("Iveskite pavarde"); 
         int age = inInt("Iveskite amziu");
         int gen = inInt("Pasirinkite lyti: 1-vyras, 2-moteris,3-kita");
         Person asmuo = new Person(name, surname, Gender.getById(gen));
         out(asmuo);
          
     }
     
    private static Banknotas suraskBanknota(int sk) {
        Banknotas result = null;
        for ( Banknotas bnkn : Banknotas.values()) {
             if (bnkn.getSk()== sk) {
                 result = bnkn;
                 break;
             }
         }
        return result;
    }
    
    
}