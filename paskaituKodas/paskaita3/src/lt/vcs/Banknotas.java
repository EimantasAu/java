
package lt.vcs;


public enum Banknotas {
    
    PENKI(5, "penki"),
    DESIMT(10, "desimt"),
    DVIM(20, "Dvidesimt"),
    PEM(50, "penkiasdesimt"),
    SIMTAS(100, "simtas");
    
    private int sk;
    private String label;
    
    private Banknotas(int sk, String label) {
        this.sk = sk;
        this.label = label;
    }

    public int getSk() {
        return sk;
    }

    public void setSk(int sk) {
        this.sk = sk;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
}
