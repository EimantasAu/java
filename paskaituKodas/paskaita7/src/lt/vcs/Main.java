
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author af
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String words = inLine("Iveskite zodzius, atskirtus kableliu");
        String[] wordMas = words.replaceAll(" ", "").split(",");
        List<String> pvz = new ArrayList();
        pvz.add("Kazkas");
        out("Nesurusiuoti zodziai: ");
         for (String word : listas) {
            out(word);
        }
        
        Collections.sort(listas);
        Set<String> setas = new TreeSet(listas);
        out("Surusiuoti zodziai: ");
         for (String word : setas) {
            out(word);
        }
         Map<String, List<String>> mapas = new HashMap();
         List<String> neSu = Arrays.asList("bananas" , "ananasas", "agurkas");
         mapas.put("nesurusiuotas", neSu);
         mapas.put("surusiuotas", listas);
         //out(mapas.get(2));
         for (String word : mapas.get("nesurusiuotas")) {
            out(word);
        }
    }
    
    private static List<String> toSortedList(String[] strMas) {
 
        List listas =  Arrays.asList(strMas);
        Collections.sort(listas);
        return listas;
    }
    
}
