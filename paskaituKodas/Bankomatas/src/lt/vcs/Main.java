
package lt.vcs;
import static lt.vcs.VcsUtils.*;


public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      Bankomatas bank = new Bankomatas();
      
      while() {
          
          Person per = new Person(inStr("Iveskite varda"));     //vartotojui ivesti varda
          String pass = inStr("Iveskite pin koda");
          bank.logIn(pass);
        
          /*logika
          naudotojui ivesti varda
          prisijungti prie banko sakskaitos, pin "TEST"
          pranesti naudotojui kiek pinigu liko bankomate
          paklausti kiek pinigu issiimti
          isminusuoti isimama pinigu suma is bankomato likucio
          */
      }
      //pranesti, kad bankomate neliko pinigu ir jis issijungia
    }
    
}
