
package paskaita3nd01;

import static paskaita3nd01.VcsUtils.*;


public class Paskaita3nd01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    int[] mas = new int[5];
    
    int suma = 0;
        for (int i = 0; i < 5; i++) {
            mas[i]= inInt("Ivesk " + i +" skaiciu");
            suma += mas[i];
        }
        
        out("Suma: " + suma);
        String skaiciai = "";
         for (int i :mas) {
            skaiciai += ", " +i;
        }
         out(skaiciai.replaceFirst(",", ""));
    }
    
}
