package lt.vcs;

import static lt.vcs.VcsUtils.*;




public class Vienaragis extends Gyvunas {

    @Override
    public void gyvent() {
        out("Vienaragis gyvena");
        gyventKaipGyvunui();
    }

    @Override
    public String getWorld() {
        return "land";
    }
    
}
