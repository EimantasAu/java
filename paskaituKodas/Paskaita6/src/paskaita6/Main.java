
package paskaita6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import static lt.vcs.VcsUtils.*;
public class Main {
    
    private static final String NL = System.lineSeparator();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
    
    String inPath = inStr("Iveskite failo nuoroda");
    File failas = new File(inPath);
    out("failas egzistuoja? " + failas.exists());
    FileOutputStream fos = new FileInputStream(failas);
    OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
    BufferedWriter bw = new BufferedWriter(osw);
    bw.newLine();
    bw.append("Pirmas irasymas");
    bw.flush();
    bw.close();
    
    FileInputStream fis = new FileInputStream(failas);
    InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
    BufferedReader br = new BufferedReader(isr);
    //out("Ivesto failo pirma eilute: " + br.readLine());
    //out("Ivesto failo antra eilute: " + br.readLine());
    //out("Ivesto failo trecia eilute: " + br.readLine());
    out("txt failo turinys" + NL + br.readLine());
    br.close();
    }
    
    
    
    
    
    
    
    private static String readTextFile(BufferedReader br) throws Exception {
       String result = "";
       
        String line;
        while(true) {
            line = br.readLine();
            if(line == null){
                break;
            }
             result += line;
        }
            
        return result; 
    }
}  // C:/pvz.txt